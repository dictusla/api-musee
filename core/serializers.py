from rest_framework import serializers
from core.models import Oeuvre, Note, User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class OeuvreSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Oeuvre
        fields = '__all__'


class NoteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Note
        fields = '__all__'
